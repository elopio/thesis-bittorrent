\documentclass[10pt]{beamer}

\usepackage[utf8]{inputenc}
% Language support
\usepackage[spanish]{babel}

\usetheme[progressbar=frametitle, subsectionpage=progressbar]{metropolis}

\graphicspath{{images/}}
\usepackage{svg}

\usepackage[scale=2]{ccicons}

\usepackage[backend=biber, style=authoryear]{biblatex}
\addbibresource{references.bib}

\newcounter{saveenumi}
\newcommand{\seti}{\setcounter{saveenumi}{\value{enumi}}}
\newcommand{\conti}{\setcounter{enumi}{\value{saveenumi}}}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\title{Propuesta de tesis}
\subtitle{Caracterización e inducción de comportamientos emergentes en redes de pares}
\date{\today}
\author{Leo Arias}
\institute{
  Instituto Tecnológico de Costa Rica \\
  Escuela de Ingeniería en Computación \\
  Programa de Maestría en Computación
}
\titlegraphic{\hfill\includegraphics[height=1cm]{logo-tec.png}}

\begin{document}

\maketitle

\begin{frame}
  \begin{center}
    \includesvg{logo-napster.svg}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[height=0.8\textheight]{megaupload.png}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[height=0.8\textheight]{logo-thepiratebay.png}
  \end{center}
\end{frame}

\begin{frame}{BitTorrent}
  \begin{center}
    \includegraphics[height=0.7\textheight]{bittorrent-upstream-trend.png} \\
    \footnotesize Imagen tomada de \parencite{cullen2018}
  \end{center}
\end{frame}

\begin{frame}
  «Nuestro pasado ha sido una cultura libre, pero sólo
  será nuestro futuro si cambiamos la ruta en la que estamos
  ahora.»
  \begin{flushright}
    Lawrence Lessig
  \end{flushright}
\end{frame}

\begin{frame}{Tabla de contenidos}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents
\end{frame}

\section{Introducción}

\subsection{Marco teórico}

\begin{frame}{Sistemas distribuidos}
  Componentes conectados a través de una red de
  computadoras que se comunican pasando mensajes con el
  objetivo de compartir recursos.
  \begin{flushright}
    \parencite{coulouris2011distributed}
  \end{flushright}
  \begin{center}
    \includegraphics[height=0.4\textheight]{web.png}
  \end{center}
\end{frame}

\begin{frame}{Sistemas distribuidos de redes de pares}
  Aplicaciones que explotan los recursos disponibles en
  los bordes de la Internet – almacenamiento, ciclos, contenido,
  presencia humana.
  \begin{flushright}
    \parencite{shirky2000}
  \end{flushright}
\end{frame}

\begin{frame}{Sistemas distribuidos de redes de pares}
  \begin{itemize}
    \item Cada usuaria contribuye recursos al sistema.
    \item Todos los nodos tienen las mismas capacidades y
      responsabilidades.
    \item La operación correcta no depende de un sistema de
      administración central.
    \item Pueden ofrecer anonimato limitado para proveedores y
      usuarias de recursos.
    \item La ubicación de los recursos debe balancear la carga de
trabajo y asegurar la disponibilidad.
  \end{itemize}
  \begin{flushright}
    \parencite{coulouris2011distributed}
  \end{flushright}
\end{frame}

\begin{frame}{Sistemas distribuidos de redes de pares - Los pares}
  \begin{itemize}
    \item Interactúan como iguales.
    \item Son \textbf{clientes} cuando accesan la información, \textbf{servidores}
      cuando la sirven a otros clientes y \textbf{enrutadores} cuando la
      redirigen.
    \item Su conexión al sistema es intermitente.
    \item No son confiables.
  \end{itemize}
  \begin{center}
    \includegraphics[height=0.4\textheight]{p2p-network.png} \\
    \footnotesize Imagen tomada de \parencite{wiki:peer-to-peer}
  \end{center}
\end{frame}

\begin{frame}{Sistemas distribuidos de redes de pares - Garantías en el caos}
  \begin{itemize}
    \item \textbf{Disponibilidad}.
    \item \textbf{Durabilidad}.
    \item Control de acceso.
    \item Autenticidad.
    \item Resistencia a ataques de negación de servicio.
    \item \textbf{Rendimiento aceptable}.
    \item \textbf{Escalabilidad masiva}.
    \item Anonimato.
    \item Negación plausible.
    \item Resistencia a la censura.
  \end{itemize}
  \begin{flushright}
    \parencite{Kubiatowicz:2003:EGC:606272.606297}
  \end{flushright}
\end{frame}

\begin{frame}{BitTorrent}
  \begin{itemize}
    \item Distribuye el costo de la subida entre quienes descargan.
    \item Conforme se descarga un archivo, todos los pares
      comparten piezas del mismo archivo.
    \item El número de descargas completas aumenta lentamente,
      llega a su punto más alto y luego cae exponencialmente.
  \end{itemize}
  \begin{flushright}
    \parencite{Cohen03incentivesbuild}
  \end{flushright}
\end{frame}

\begin{frame}{BitTorrent - El enjambre}
  \begin{center}
    \includegraphics[height=0.7\textheight]{leech-bittorrent-label.png}
    \footnotesize Imagen tomada de \parencite{wiki:peer-to-peer}
  \end{center}
\end{frame}

\begin{frame}{BitTorrent - Ahogamiento}
  \begin{itemize}
    \item Cada par es responsable de maximizar su tasa de
      descarga.
    \item Descargar de quién esté disponible.
    \item Para decidir a cuáles pares enviar datos...
      \begin{itemize}
        \item Siempre desahoga un número fijo de pares en periodos
          de 10 segundos, basándose en la tasa de descarga actual.
        \item Siempre mantiene un desahogado optimista que se
          cambia cada 30 segundos, sin importar la tasa de
          descarga.
       \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Descripción del problema}

\begin{frame}{Descripción del problema}
  \begin{itemize}
    \item Disponibilidad, durabilidad y eficiencia dependen de los
      pares conectados.
    \item Conflicto entre premura por la descarga y renuencia a la
      subida.
    \item \textbf{Es necesario incentivar a los pares que poseen
      piezas completas para que se mantengan
      compartiéndolas, sin hacer al sistema más difícil de
      uar ni menos libre.}
  \end{itemize}
\end{frame}

\subsection{Hipótesis}

\begin{frame}{Hipótesis}
  \emph{Aunque el comportamiento de un individuo miembro de un
    sistema distribuido de red de pares es esencialmente
    impredecible (incluida su misma presencia), el sistema como
    un todo muestra propiedades emergentes que pueden ser
    predecidas y controladas por políticas externas a este.}
\end{frame}

\section{Antecedentes}

\begin{frame}{Antecedentes}
  Entre más compartan las sanguijuelas, el tiempo de
  descarga promedio se reduce, porque el archivo se
  comparte de forma más eficiente.
  \begin{flushright}
    \parencite{Qiu:2004:MPA:1015467.1015508}
  \end{flushright}
\end{frame}

\begin{frame}{Antecedentes}
  \begin{itemize}
    \item En el 14\% de los casos, las sanguijuelas pueden
      reconstruir el archivo sin que haya semillas presentes.
    \item En el 64\% de los torrents, los periodos de indisponibilidad
      no son permanentes.
    \item El 23\% de los casos en que no hay semillas logran
      completar sus descargas.
    \item Al abandonar una descarga incompleta, las usuarias
      causan una reacción en cadena que aumenta la
      indisponibilidad.
  \end{itemize}
  \begin{flushright}
    \parencite{DBLP:conf/p2p/KauneRTMGS10}
  \end{flushright}
\end{frame}

\begin{frame}{Antecedentes}
  Torrents resistentes sobreviven en ausencia de semillas.
  \begin{itemize}
    \item Población de sanguijuelas grande desde el inicio.
    \item Factor de agitamiento bajo.
    \item Tasa de semillas / sanguijuelas alta.
  \end{itemize}
  \begin{flushright}
    \parencite{DBLP:conf/p2p/KauneRTMGS10}
  \end{flushright}
\end{frame}

\begin{frame}{Antecedentes}
  \begin{itemize}
    \item El efecto dominante en el rendimiento es la contribución
      altruista de una pequeña minoría de pares de alta
      capacidad.
    \item Este altruismo no es consecuencia del algoritmo de
      reciprocidad propuesto por el protocolo.
  \end{itemize}
  \begin{flushright}
    \parencite{Piatek07doincentives}
  \end{flushright}
\end{frame}

\begin{frame}{Antecedentes}
  \begin{itemize}
    \item La disponibilidad no puede ser solucionada modificando
      los algoritmos de BitTorrent.
    \item Se debe incentivar a las usuarias para que modifiquen su
      comportamiento.
  \end{itemize}
  \begin{flushright}
    \parencite{DBLP:conf/p2p/KauneRTMGS10}
  \end{flushright}
\end{frame}

\section{Objetivos y contribuciones}

\begin{frame}{Objetivo general}
  Analizar posibles incentivos para las usuarias del sistema
  \emph{BitTorrent} que aumenten la disponibilidad de
  los archivos compartidos, y que mantengan la
  facilidad de uso y libertad de este protocolo.
\end{frame}

\begin{frame}{Objetivos específicos}
  \begin{itemize}
    \item Recolectar y analizar datos estadísticos sobre el uso y
      rendimiento de \emph{BitTorrent} en la vida real.
    \item Modelar las interacciones y estrategias entre los pares al
      compartir y descargar un archivo, y al unirse y separarse
      de la red.
    \item Analizar el comportamiento emergente de la red e
      identificar las características que indican deterioro en el
      rendimiento y la posible pérdida de un archivo.
  \end{itemize}
\end{frame}

\begin{frame}{Objetivos específicos}
  \begin{itemize}
    \item Proponer incentivos que aumenten la disponibilidad y
      velocidad de descarga de los archivos compartidos en
      \emph{BitTorrent}.
    \item Implementar una simulación que permita analizar el
      rendimiento de \emph{BitTorrent} con los cambios propuestos.
    \item Diseñar experimentos para comprobar el efecto de los
      incentivos propuestos con usuarias reales.
  \end{itemize}
\end{frame}

\begin{frame}{Contribuciones}
  \begin{itemize}
    \item Un resumen de los enfoques utilizados para recolectar,
      modelar y analizar las interacciones complejas entre los
      miembros de una red de pares.
    \item Una mejor comprensión de las acciones de los pares dentro
      de la red, así como sus expectativas y motivaciones.
    \item Discusión y análisis sobre qué políticas implementar según
      el objetivo de la red, en qué momento son necesarias, y
      qué cambios esperar de estas.
  \end{itemize}
\end{frame}

\begin{frame}{Contribuciones}
  \begin{itemize}
    \item Una simulación para poner a prueba nuevas ideas y
      predecir sus resultados.
    \item Base para trabajo futuro en otras áreas en las que el
      altruismo y la cooperación entre miembros del sistema
      sean esenciales para su éxito.
  \end{itemize}
\end{frame}

\begin{frame}{Alcances y limitaciones}
  \begin{itemize}
    \item Las modificaciones e incentivos al protocolo BitTorrent no
      alterarán los algoritmos de selección de piezas ni de
      selección de pares.
    \item No involucrarán dinero o puntos.
    \item No habrá ningún castigo adicional para los pares que no
      compartan o decidan ignorar los incentivos.
  \end{itemize}
\end{frame}

\begin{frame}{Alcances y limitaciones}
  Este trabajo no cubrirá los siguientes temas (estos pueden ser
  explorados en trabajos futuros):
  \begin{itemize}
    \item Leyes de derechos de autor.
    \item Anonimato, cifrado, negación plausible y resistencia a la
      censura.
    \item Redes pequeñas de \emph{BitTorrent}.
    \item Asegurar disponibilidad permanente de los archivos.
  \end{itemize}
\end{frame}

\section{Metodología y cronograma}

\begin{frame}{Metodología}
  \begin{itemize}
    \item El algoritmo de ahogamiento es una estrategia óptima
      para el dilema iterativo del prisionero en \textbf{Teoría de
        Juegos}.
    \item \textbf{Simulaciones computacionales} para analizar las
      propiedades no visibles en los sistemas reales.
    \item \textbf{Diseño de experimentos} controlados para probar la
      validez estadística de los efectos observados al aplicar los
      distintos tratamientos.
  \end{itemize}
\end{frame}

\begin{frame}{Cronograma}
  \begin{enumerate}
    \item Investigar la historia de otros sistemas de intercambio de archivos.
      \textbf{1 mes}
    \item Investigar trabajos anteriores sobre incentivos de \emph{BitTorrent}.
      \textbf{2 semanas}
    \item Investigar trabajos anteriores sobre modelos de \emph{BitTorrent}.
      \textbf{2 semanas}
    \item Monitorear enjambres de \emph{BitTorrent} para recolectar datos de
      resiliencia. \textbf{1 mes}
    \item Investigar métodos de simulación de redes de pares. \textbf{1 mes}
    \seti
  \end{enumerate}
\end{frame}

\begin{frame}{Cronograma}
  \begin{enumerate}
    \conti
    \item Identificar y analizar propiedades emergentes de la red. \textbf{1 mes}
    \item Proponer y analizar incentivos. \textbf{3 meses}
    \item Desarrollar la simulación de la red. \textbf{2 meses}
    \item Diseño y aplicación de experimentos. \textbf {2 meses}
    \item Preparación del documento y presentación de tesis.
  \end{enumerate}
\end{frame}

\begin{frame}[allowframebreaks]
  \printbibliography
\end{frame}

{\setbeamercolor{palette primary}{fg=black, bg=yellow}
  \begin{frame}[standout]
    \begin{center}
      Muchas gracias \\
      \ccbysa \\
      ¿Preguntas?
    \end{center}
  \end{frame}
}

\end{document}
